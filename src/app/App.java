/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.List;
import model.Profesor;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Maite
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //CREAMOS CONEXION
        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        configuration.configure();
        sessionFactory = configuration.buildSessionFactory();
        SessionFactory factory = new Configuration().configure().buildSessionFactory();

        //CREAR UNA SESION
        Session session = factory.openSession();

        Query query = session.createQuery("SELECT p FROM Profesor p");

        List<Profesor> listaProfesores = query.list();

        for (Profesor prof : listaProfesores) {

            System.out.println(prof);

        }

        query = session.createQuery("SELECT p.id, p.nombre FROM Profesor p");
        List<Object[]> listDatos = query.list();
        
        for(Object[] o : listDatos){
            
            System.out.println(o[0] + " -- " + o[1]);
            
        }
        
        query = session.createQuery("SELECT p.nombre FROM Profesor p");
        List<Object> listNombre = query.list();
        
        for(Object datos : listNombre){
            System.out.println(datos);
        }
        
        session.close();
        factory.close();

    }

}
